= Forming question

In Estonian, there are several ways to form questions, depending on the type and structure of the question. Here are some common methods:

Intonation: Simply raise your intonation at the end of a statement to turn it into a yes/no question.

_Example: "Sa tuled?" (Are you coming?)_

== Question Words: Use specific interrogative words to form questions.

|===
|English |Estonian|Example|

|Who
|Kes
|Kes siin on?
|Kelle/kellede keda/keda

|Where
|Kus
|Kus sa elad
|

|What
|Mis/Mida
|Mis see tähendab?/ Mis see on?
|mille/millede mida/mida

|Why
|Miks
|Miks lumi on valge?
|

|How
|Kuidas
|Kuidas läheb?
|

|Which
|Milline
|Milline on sinu?
|milline/millised millise/milliste millist/milliseid

|When
|Millal
|Millal sa tuled?
|

|Can
|Kas
|Kas sa tuled?
|

|===

== Word Order
Changing the word order can also indicate a question, especially in more complex sentences.

_Example: "Sa tuled õhtul?" (Are you coming in the evening?)_

== Adding a Question Particle
Sometimes, a question particle like "ju" can be added for emphasis.

_Example: "Sa tuled ju?" (You are coming, right?)_

== Using "kas"
For yes/no questions, you can start the question with "kas."

_Example: "Kas sa tuled?" (Are you coming?)_
